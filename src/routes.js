const express = require('express')
const cartasController = require('./controllers/cartasController')

const routes = express.Router()

routes.post('/cartas', cartasController.createCartas)
routes.get('/cartas/:id', cartasController.getCartasById)
routes.get('/cartas/', cartasController.getAllCartas)

module.exports = routes