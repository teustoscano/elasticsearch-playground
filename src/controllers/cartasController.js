// {
//     valor:
//     adm:
//     valor:
//     entrada:
//     parcelas:
//     qtd_parcelas:
//     data_parcela:
// }

const elastic = require('../elastic')

defaultObj = () => {
    return {
        index: 'carta',
        type: 'type_carta',
    }
}

module.exports = {
    async createCartas(req, res) {

        let obj = defaultObj()
        obj.body = req.body
        elastic.index(obj, (err) => {
            if (err) {
                return res.status(500).json({ msg: 'Erro' })
            } else {
                return res.status(200).json({ msg: 'Item adicionado' })
            }
        })
    },

    async getCartasById(req, res) {

        let obj = defaultObj()
        obj.id = req.params.id

        elastic.get(obj, (err, response) => {
            if (err) {
                return res.status(500).json({ msg: 'Erro' })
            } else {
                return res.status(200).json({ carta: response._source })
            }
        })
    },

    async getAllCartas(req, res) {

        let obj = defaultObj()
        obj.body = {
            query:{
                match_all: {}
            }
        }
        elastic.search(obj, (err, response) => {
            if (err) {
                return res.status(500).json({ msg: 'Erro' })
            } else {
                return res.status(200).json({ carta: response.hits })
            }
        })
    },
}